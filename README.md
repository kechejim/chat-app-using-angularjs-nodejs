# MEAN Chat APP using Bootstrap
author: Jim Klinger
Date: 2015.1.8

## Demo site

[http://mean-chat.herokuapp.com/](http://mean-chat.herokuapp.com/)



## Running the app

Run 'grunt:server' to start the app in development mode, or run 'grunt server:dist' to run it in production mode.

## License

Jim Klinger

